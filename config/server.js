var express = require('express');
var consign = require('consign');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var expressSession = require('express-session');

var app = express();
app.set('view engine', 'ejs');
app.set('views', './app/views');

app.use(express.static('./app/public'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(expressValidator());
app.use(expressSession({
	secret: 'peladaItau10Secret',
	resave: false,
	saveUninitialized: false,
	maxAge: Date.now() + (30 * 86400 * 1000)
}));


// Com este middleware, a variável usuarioLogado fica acessível em todos os templates.
app.use(function(req, res, next){
	res.locals.usuarioLogado = req.session.usuarioLogado;
	next();
});

consign()
	.include('app/routes')
	.then('config/dbConnection.js')
	.then('app/dal')
	.then('app/util')
	.then('app/controllers')
	.into(app);

module.exports = app;