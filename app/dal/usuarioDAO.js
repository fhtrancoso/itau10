
function getListaUsuarios(req, connection){
	
	return new Promise(function(resolve, reject){
		let query = 'select id, ' +
						'nome, ' +
						'telefone, ' +
						'CASE ' +
							'WHEN perfil = "A" THEN "ADMINISTRADOR" ' +
							'WHEN perfil = "N" THEN "NORMAL" ' +
							'ELSE "Indefinido" ' +
						'END AS perfil from usuario order by nome';

		connection.query(query, function(erro, result){
			if(erro){					
				reject(erro);
			}

			resolve(result);
			connection.end();
			console.log('Conexão getListaUsuarios encerrada');
		});
	});	
}

function getUsuarioPorId(idUsuario, connection){
	
	return new Promise(function(resolve, reject){
		connection.query('select u.id, u.nome, u.telefone, u.perfil from usuario u where u.id = ?', [idUsuario], function(erro, result){
			if(erro){					
				reject(erro);
			}

			resolve(result[0]);
			connection.end();
			console.log('Conexão getUsuarioPorId encerrada');
		});
	});	
}

function getUsuarioPorTelefoneSenha(telefone, senha, connection){
	
	return new Promise(function(resolve, reject){
		connection.query('select u.id, u.nome, u.telefone, u.perfil, u.primeiro_acesso ' +
							'from usuario u where u.telefone = ? and u.senha = ?;', 
			[telefone, senha], function(erro, result){
			if(erro){					
				reject(erro);
			}

			resolve(result[0]);
			connection.end();
			console.log('Conexão getUsuarioPorTelefoneSenha encerrada');
		});
	});	
}

function salvarUsuario(connection, usuario){
	if(usuario.id){
		return new Promise(function (resolve, reject){
			connection.query('update usuario set nome = ?, telefone = ?, perfil = ? where id = ?; ', 
				[usuario.nome, usuario.telefone, usuario.perfil, usuario.id], function(erro, result){
					if(erro){
						reject(erro);
					}
	
					resolve(result);
					connection.end();
					console.log('Conexão salvarUsuario encerrada');
				});	
		});
	}else{
		return new Promise(function (resolve, reject){
			connection.query('insert into usuario (telefone, senha, nome, pelada_id, primeiro_acesso, perfil) values (?, ?, ?, ?, ?, ?) ', 
				[usuario.telefone, usuario.senha, usuario.nome, usuario.pelada_id, usuario.primeiro_acesso, usuario.perfil], function(erro, result){
					if(erro){
						reject(erro);
					}
	
					resolve(result);
					connection.end();
					console.log('Conexão salvarUsuario encerrada');
				});	
		});
	}

}

function deletarPresencasDoUsuario(connection, idUsuario, fecharConexao){
	
	return new Promise(function(resolve, reject){
		connection.query('delete from presenca_usuario where usuario_id = ?; ', 
			[idUsuario], function(error, result){
				if(error){					
					reject(error);
				}

				resolve(result);
				if(fecharConexao){
					connection.end();
					console.log('Conexão deletarPresencasDoUsuario encerrada');
				}
			});
	});
}

function deletarUsuario(connection, idUsuario, fecharConexao){
	return new Promise(function(resolve, reject){
		connection.query('delete from usuario where id = ?; ', 
			[idUsuario], function(error, result){
				if(error){					
					reject(error);
				}

				resolve(result);
				if(fecharConexao){
					connection.end();
					console.log('Conexão deletarUsuario encerrada');
				}
			});
	});
}

function deletarUsuarioTransacional(connection, idUsuario){
	
	return new Promise (function(resolve, reject){
		connection.beginTransaction(function(error) {
			
			deletarPresencasDoUsuario(connection, idUsuario, false)
			.then(function(result){
				deletarUsuario(connection, idUsuario, false)
				.then(function(resultado){

					console.log('REALIZANDO O COMMIT... ');
					connection.commit(function(error){
						if (error){
							reject(error);
						}
					});

					resolve(resultado);
					connection.end();
					console.log('Conexão deletarUsuarioTransacional encerrada');
				})
				.catch(function(error){
					connection.rollback(function() {
						console.log('Roolback deletarUsuario realizado.');
						console.log(error);
					});
					reject(error);
				})	
			})
			.catch(function(error){
				connection.rollback(function() {
					console.log('Roolback deletarPresencasDoUsuario realizado.');
				});
				reject(error);
			});
		});
	});
}

function alterarSenha(connection, idUsuario, novaSenha, primeiroAcesso){
	return new Promise(function (resolve, reject){
		connection.query('update usuario set senha = ?, primeiro_acesso = ? where id = ?; ', 
			[novaSenha, primeiroAcesso, idUsuario], function(erro, result){
				if(erro){
					reject(erro);
				}

				resolve();
				connection.end();
				console.log('Conexão alterarSenha encerrada');
			});	
	});
}

module.exports.getListaUsuarios = getListaUsuarios;
module.exports.getUsuarioPorId = getUsuarioPorId;
module.exports.getUsuarioPorTelefoneSenha = getUsuarioPorTelefoneSenha;
module.exports.salvarUsuario = salvarUsuario;
module.exports.deletarUsuarioTransacional = deletarUsuarioTransacional;
module.exports.alterarSenha = alterarSenha;