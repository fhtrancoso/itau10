
function inserirPresenca(connection, presenca, fecharConexao){
	return new Promise(function(resolve, reject){
		connection.query('insert into presenca (data, status, ano, pelada_id) values (?, ?, ?, ?) ', 
			[presenca.data, presenca.status, presenca.ano, presenca.pelada_id], function(error, result){
				if(error){					
					reject(error);
				}

				resolve(result);
				if(fecharConexao){
					connection.end();
					console.log('Conexão inserirPresenca encerrada');
				}
			})
	});
}

function inserirPresencaUsuarios(connection, idPresenca, fecharConexao){
	return new Promise(function(resolve, reject){

		connection.query("INSERT INTO presenca_usuario (presente, atrasado, suspenso, presenca_id, usuario_id) SELECT 0, 0, 0, ?, id FROM usuario; ", 
			[idPresenca], function(error, result){
						
				if(error){					
					reject(error);
				}

				resolve(result);
				if(fecharConexao){
					connection.end();
					console.log('Conexão inserirPresencaUsuarios encerrada');
				}
			});
	});
}

function inserirListaPresencaTransacional(connection, presenca){

	return new Promise (function(resolve, reject){
		connection.beginTransaction(function(error) {
			
			inserirPresenca(connection, presenca, false)
				.then(function(result){
					inserirPresencaUsuarios(connection, result.insertId, false)
						.then(function(resultado){

							console.log('REALIZANDO O COMMIT... ');
							connection.commit(function(error){
								if (error){
									reject(error);
								}
							});

							resolve(resultado);
							connection.end();
							console.log('Conexão inserirListaPresencaTransacional encerrada');
						})
						.catch(function(error){
							connection.rollback(function() {
								console.log('Roolback inserirPresencaUsuarios realizado.');
								console.log(error);
							});
							reject(error);
						})	
				})
				.catch(function(error){
					console.log(error);
					connection.rollback(function() {
						console.log('Roolback inserirPresenca realizado.');
					});
					reject(error);
				})
		})
	}) 
}

// Obtém a lista de PRESENCA
function getListaPresencas(req, connection){
	
	return new Promise(function(resolve, reject){

		let queryStr = 'select id, DATE_FORMAT(data,"%d/%m/%Y") AS data, ' +
					'CASE ' + 
						'WHEN status = "A" THEN "ABERTA" ' + 
						'WHEN status = "F" THEN "FECHADA" ' +
						'ELSE " - " ' +
					'END status from presenca order by id desc';
		console.log(queryStr);
		connection.query(queryStr, function(erro, result){
			if(erro){					
				reject(erro);
			}
			resolve(result);
			connection.end();
			console.log('Conexão getListaPresencas encerrada');
		});
	});	
}

// Obtém a lista de PRESENCA_USUARIO
function getPresencaPorId(idPresenca, connection){
	
	return new Promise(function(resolve, reject){

		let queryStr = 'select id, DATE_FORMAT(data,"%d/%m/%Y") AS data, ' +
					'CASE ' + 
						'WHEN status = "A" THEN "ABERTA" ' + 
						'WHEN status = "F" THEN "FECHADA" ' +
						'ELSE " - " ' +
					'END status, ano AS ano ' +
					'from presenca where id = ? order by data desc';
		connection.query(queryStr, [idPresenca], function(erro, result){
			if(erro){					
				reject(erro);
			}

			resolve(result[0]);
			connection.end();
			console.log('Conexão getPresencaPorId encerrada');
		});
	});	
}

function getPresencaAberta(connection){
	
	return new Promise(function(resolve, reject){

		let queryStr = 'select id, date_format(data,"%d/%m/%y") as data, ' +
					'case ' + 
						'when status = "a" then "aberta" ' + 
						'when status = "f" then "fechado" ' +
						'else " - " ' +
					'end status, ano as ano ' +
					'from presenca where status = "a" order by data; ';
		connection.query(queryStr, function(erro, result){
			if(erro){					
				reject(erro);
			}

			if(result != undefined && result.length > 0){
				resolve(result[0]);
			}else{
				resolve(null);
			}
			connection.end();
			console.log('Conexão getPresencaAberta encerrada');
		});
	});	
}

function validaDataPresencaExistente(connection, data){
	
	return new Promise(function(resolve, reject){
		let queryStr = 'select * from presenca where data = ?; ';
		connection.query(queryStr, [data], function(erro, result){
			if(erro){					
				reject(erro);
			}
			
			if(result != undefined && result.length > 0){
				resolve(true);
			}else{
				resolve(false);
			}
			connection.end();
			console.log('Conexão validaDataPresencaExistente encerrada');
		});
	});	
}

function getListaUsuariosPresenca(idPresenca, connection){
	
	return new Promise(function(resolve, reject){

		let queryStr = 'select ' +
						'	pu.id as idPresencaUsuario, ' +
						'	u.nome as nome, ' +
						'	pu.presente as presente, ' +
						'	pu.atrasado as atrasado, ' +
						'	pu.suspenso as suspenso ' +
						'from presenca_usuario pu ' +
						'	join usuario u on pu.usuario_id = u.id ' +
						'where pu.presenca_id = ? order by u.nome';
		
		connection.query(queryStr, [idPresenca], function(erro, result){
			if(erro){					
				reject(erro);
			}
			resolve(result);
			connection.end();
			console.log('Conexão getListaUsuariosPresenca encerrada');
		});
	});	
}

// Método que seta todos os atributos de presença, atrasado e suspenso de todos usuários da presença.
function zerarFlagsUsuariosPresenca(connection, idPresenca, fecharConexao){

	return new Promise(function(resolve, reject){
		connection.query('update presenca_usuario set presente = 0, atrasado = 0, suspenso = 0 where presenca_id = ?; ', 
			[idPresenca], function(error, result){
				if(error){					
					reject(error);
				}

				resolve();
				if(fecharConexao){
					connection.end();
					console.log('Conexão zerarFlagsUsuariosPresenca encerrada');
				}
			});
	});
}


// Método para atualizar os registros de usuario_presenca dos usuários presentes
function atualizarPresentes(connection, presentes, fecharConexao){

	return new Promise(function(resolve, reject){
		connection.query('update presenca_usuario set presente = 1 where id in (?); ', 
			[presentes], function(error, result){
				if(error){					
					reject(error);
				}

				resolve();
				if(fecharConexao){
					connection.end();
					console.log('Conexão atualizarPresentes encerrada');
				}
			});
	});
}

// Método para atualizar os registros de usuario_presenca dos usuários atrasados
function atualizarAtrasados(connection, atrasados, fecharConexao){

	return new Promise(function(resolve, reject){
		connection.query('update presenca_usuario set atrasado = 1 where id in (?); ', 
			[atrasados], function(error, result){
				if(error){					
					reject(error);
				}

				resolve();
				if(fecharConexao){
					connection.end();
					console.log('Conexão atualizarAtrasados encerrada');
				}
			});
	});
}

// Método para atualizar os registros de usuario_presenca dos usuários suspensos
function atualizarSuspensos(connection, suspensos, fecharConexao){

	return new Promise(function(resolve, reject){
		connection.query('update presenca_usuario set suspenso = 1 where id in (?); ', 
			[suspensos], function(error, result){
				if(error){					
					reject(error);
				}

				resolve();
				if(fecharConexao){
					connection.end();
					console.log('Conexão atualizarSuspensos encerrada');
				}
			});
	});
}

// método transacional para atualizar os status de presentes, atrasados e suspensos de determinada presença (data)
function salvarListaUsuariosPresencaTransacional(connection, itensSelecionados){

	return new Promise (function(resolve, reject){
		connection.beginTransaction(function(error) {
console.log('CELULAR 001');
console.log(itensSelecionados);
			zerarFlagsUsuariosPresenca(connection, itensSelecionados.idPresenca, false)
			.then(function(){
				atualizarPresentes(connection, itensSelecionados.presente, false)
				.then(function(){
					atualizarAtrasados(connection, itensSelecionados.atrasado, false)
					.then(function(){
						atualizarSuspensos(connection, itensSelecionados.suspenso, false)
						.then(function(){
							console.log('REALIZANDO O COMMIT... ');
							connection.commit(function(error){
								if (error){
									reject(error);
								}
							});
							resolve();
							connection.end();
							console.log('Conexão salvarListaUsuariosPresencaTransacional encerrada');
						})
						.catch(function(error){
							connection.rollback(function() {
								console.log('Roolback atualizarSuspensos realizado.');
								console.log(error);
							});
							reject(error);
						})
					})
					.catch(function(error){
						connection.rollback(function() {
							console.log('Roolback atualizarAtrasados realizado.');
							console.log(error);
						});
						reject(error);
					})
				})
				.catch(function(error){
					connection.rollback(function() {
						console.log('Roolback atualizarPrentes realizado.');
						console.log(error);
					});
					reject(error);
				})
			})
			.catch(function(error){
				connection.rollback(function() {
					console.log('Roolback zerarFlagsUsuariosPresenca realizado.');
					console.log(error);
				});
				reject(error);
			})					
		})
	})			
}

// Método para obter a consulta da página inicial
// Relatório de presenças e percentuais
function getRelatorioPresencas(anoBase, connection){
	
	let queryStr = 'CALL PR_RELATORIO_PRESENCAS(?);';

	return new Promise(function(resolve, reject){

		connection.query(queryStr, [anoBase], function(erro, result){
			if(erro){					
				reject(erro);
			}

			// Neste caso de proc, o objeto de retorno é um array contendo o select 
			//da proc e um objeto de Retorno OK.
			resolve(result[0]);
			connection.end();
			console.log('Conexão getRelatorioPresencas encerrada');
		});
	});	
}

// Método para obter a lista de escalação, após abertura de presença do dia atual
function getRelatorioEscalacao(anoBase, connection){
	
	let queryStr = 'CALL PR_RELATORIO_ESCALACAO(?);';

	return new Promise(function(resolve, reject){

		connection.query(queryStr, [anoBase], function(erro, result){
			if(erro){					
				reject(erro);
			}

			// Neste caso de proc, o objeto de retorno é um array contendo o select 
			//da proc e um objeto de Retorno OK.
			resolve(result[0]);
			connection.end();
			console.log('Conexão getRelatorioEscalacao encerrada');
		});
	});	
}

// Método para atualizar o Status da presença para Fechada ou Aberta.
function atualizarStatusPresenca(connection, status, idPresenca){

	return new Promise(function(resolve, reject){
		connection.query('update presenca set status = ? where id = ?; ', 
			[status, idPresenca], function(error, result){
				if(error){					
					reject(error);
				}

				resolve();
				connection.end();
				console.log('Conexão atualizarStatusPresenca encerrada');
			})
	});
}

function get10PresencasUsuario(idUsuario, connection){
	
	return new Promise(function(resolve, reject){

		let queryStr = 'select ' + 
							'DATE_FORMAT(data,"%d/%m/%Y") AS data, ' +
							'CASE ' +
								'WHEN pu.suspenso = 1 THEN "SUSPENSO" ' +
								'WHEN pu.atrasado = 1 THEN "ATRASADO" ' +
								'WHEN pu.presente = 1 AND pu.atrasado = 0 AND pu.suspenso = 0 THEN "PRESENTE" ' +
								'WHEN pu.presente = 0 and pu.atrasado = 0 and pu.suspenso = 0 THEN "FALTA" ' +
								'ELSE " - " ' +
							'END status ' +
						'from presenca_usuario pu ' +
							'join presenca p on p.id = pu.presenca_id ' +
						'where pu.usuario_id = ? ' +
						'order by p.data desc ' +
						'limit 10';
		console.log(queryStr);
		connection.query(queryStr, [idUsuario], function(erro, result){
			if(erro){					
				reject(erro);
			}
			resolve(result);
			connection.end();
			console.log('Conexão get10PresencasUsuario encerrada');
		});
	});	
}


module.exports.getRelatorioPresencas = getRelatorioPresencas;
module.exports.inserirListaPresencaTransacional = inserirListaPresencaTransacional;
module.exports.getListaPresencas = getListaPresencas;
module.exports.getPresencaPorId = getPresencaPorId;
module.exports.getListaUsuariosPresenca = getListaUsuariosPresenca;
module.exports.salvarListaUsuariosPresencaTransacional = salvarListaUsuariosPresencaTransacional;
module.exports.atualizarStatusPresenca = atualizarStatusPresenca;
module.exports.getPresencaAberta = getPresencaAberta;
module.exports.validaDataPresencaExistente = validaDataPresencaExistente;
module.exports.getRelatorioEscalacao = getRelatorioEscalacao;
module.exports.get10PresencasUsuario = get10PresencasUsuario;