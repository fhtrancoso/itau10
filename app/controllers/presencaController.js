// Carregar tela
module.exports.listarPresencas = function(application, req, res){

	var connection = application.config.dbConnection();
	
	application.app.dal.presencaDAO.getListaPresencas(req, connection)
		.then(function(presencas){

			var mensagemAlerta;
			res.render("presenca/listaPresencas", {listaPresencas : presencas, mensagemAlerta: mensagemAlerta});
        })
		.catch(function(erro){
            console.log(erro);
			var mensagemAlerta = new application.app.util.MensagemAlerta(0, [{msg: 'Erro interno!!!!'}]);
			res.render("presenca/listaPresencas", {mensagemAlerta : mensagemAlerta, listaPresencas : []});
		});
} 

// Carregar tela
module.exports.listarUsuariosPresenca = function(application, req, res){
	
	var idPresenca = req.query.idPresenca;

	var connection = application.config.dbConnection();
	
	application.app.dal.presencaDAO.getPresencaPorId(idPresenca, connection)
	.then(function(result){
		var presenca = result;
		
		var connection = application.config.dbConnection();
		application.app.dal.presencaDAO.getListaUsuariosPresenca(idPresenca, connection)
			.then(function(usuarios){								

				var mensagemAlerta;
				if(req.query.operSucesso){
					mensagemAlerta = new application.app.util.MensagemAlerta(1, [{msg: 'Operação realizada com sucesso.'}]);
				}
				res.render("presenca/usuariosPresenca", {usuarios : usuarios, presenca: presenca, mensagemAlerta: mensagemAlerta});
			})
			.catch(function(erro){
				console.log(erro);
				var mensagemAlerta = new application.app.util.MensagemAlerta(0, [{msg: 'Erro interno!!!!'}]);
				res.render("presenca/listaPresencas", {mensagemAlerta : mensagemAlerta, listaPresencas : []});
			});	
	});

} 

module.exports.salvarListaUsuariosPresenca = function(application, req, res){
	var itensForm = req.body;

	var connection = application.config.dbConnection();	
	application.app.dal.presencaDAO.salvarListaUsuariosPresencaTransacional(connection, itensForm)
		.then(function(usuarios){
			res.redirect('/usuariosPresenca?idPresenca=' + itensForm.idPresenca + '&operSucesso=true');
        })
		.catch(function(erro){
            console.log(erro);
			var mensagemAlerta = new application.app.util.MensagemAlerta(0, [{msg: 'Erro interno!!!!'}]);
			res.render("/usuariosPresenca", {mensagemAlerta : mensagemAlerta, usuarios : []});
		})	
} 

module.exports.listarUsuariosPresencaAberta = function(application, req, res){
	
	var connection = application.config.dbConnection();

	application.app.dal.presencaDAO.getPresencaAberta(connection)
		.then(function(presencaAberta){

			if(!presencaAberta){				
				res.redirect('/?mensagemErro=Não existe presença aberta.');				
			}else{
				var connection = application.config.dbConnection();
				application.app.dal.presencaDAO.getListaUsuariosPresenca(presencaAberta.id, connection)
				.then(function(usuarios){
					var mensagemAlerta;
					if(req.query.operSucesso){
						mensagemAlerta = new application.app.util.MensagemAlerta(1, [{msg: 'Operação realizada com sucesso.'}]);
					}
					res.render("presenca/usuariosPresenca", {usuarios : usuarios, presenca: presencaAberta, mensagemAlerta: mensagemAlerta});
				})
				.catch(function(erro){
					console.log(erro);
					var mensagemAlerta = new application.app.util.MensagemAlerta(0, [{msg: 'Erro interno!!!!'}]);
					res.render("presenca/listaPresencas", {mensagemAlerta : mensagemAlerta, listaPresencas : []});
				});	
			}
		});	
} 

module.exports.listarPresencasUsuario = function(application, req, res){
	var idUsuario = req.query.idUsuario;

	var connection = application.config.dbConnection();

	var presencas;
	application.app.dal.presencaDAO.get10PresencasUsuario(idUsuario, connection)
	.then(function(result){
		presencas = result;
		res.render("presenca/listaPresencasUsuario", {mensagemAlerta : undefined, presencas : presencas});
	})
	.catch(function(erro){
		console.log(erro);
		var mensagemAlerta = new application.app.util.MensagemAlerta(0, [{msg: 'Erro interno!!!!'}]);
		res.render("presenca/listaPresencasUsuario", {mensagemAlerta : mensagemAlerta, usuario : {}});
	});
}