module.exports.index = function(application, req, res){

	var connection = application.config.dbConnection();
	
	var anoBase = new Date().getFullYear();

	// application.app.dal.usuarioDAO.getTodosUsuarios(req, connection)
	application.app.dal.presencaDAO.getRelatorioPresencas(anoBase, connection)
		.then(function(usuarios){

			var mensagemAlerta;
			if(req.query.operSucesso){
				mensagemAlerta = new application.app.util.MensagemAlerta(1, [{msg: 'Operação realizada com sucesso.'}]);
			}else if(req.query.mensagemErro){
				mensagemAlerta = new application.app.util.MensagemAlerta(0, [{msg: req.query.mensagemErro}]);
			}

			res.render("home/index", {usuarios : usuarios, mensagemAlerta: mensagemAlerta});
		})
		.catch(function(erro){
			console.log(erro);
			var mensagemAlerta = new application.app.util.MensagemAlerta(0, [{msg: 'Erro interno!!!!'}]);
			res.render("home/index", {mensagemAlerta : mensagemAlerta, usuarios : []});
		});
} 

module.exports.escalacao = function(application, req, res){

	var connection = application.config.dbConnection();
	
	var anoBase = new Date().getFullYear();

	// application.app.dal.usuarioDAO.getTodosUsuarios(req, connection)
	application.app.dal.presencaDAO.getRelatorioEscalacao(anoBase, connection)
		.then(function(usuarios){
			res.render("home/escalacao", {usuarios : usuarios, mensagemAlerta: undefined});
		})
		.catch(function(erro){
			console.log(erro);
			var mensagemAlerta = new application.app.util.MensagemAlerta(0, [{msg: 'Erro interno!!!!'}]);
			res.render("home/escalacao", {mensagemAlerta : mensagemAlerta, usuarios : []});
		});
} 

module.exports.autenticar = function(application, req, res){
	
	var telefone = req.body.telefone;
	var senha = req.body.senha;

	telefone = telefone.replace('-', '');
	
	req.assert('telefone','Telefone é obrigatório').notEmpty();
	req.assert('telefone','Telefone deve ter 9 dígitos.').len(10, 10);
	req.assert('senha','Senha é obrigatório').notEmpty();
	req.assert('senha','Senha deve ter entre 5 e 45 dígitos.').len(5, 45);
	
	var erros = req.validationErrors();

	// Erros de validação
	if(erros){
		var mensagemAlerta = new application.app.util.MensagemAlerta(0, erros);			
		res.render("admin/login", {mensagemAlerta: mensagemAlerta});
		return;
	}

	var connection = application.config.dbConnection();

	application.app.dal.usuarioDAO.getUsuarioPorTelefoneSenha(telefone, senha, connection)
		.then(function(usuario){

			if(usuario != undefined){

				if(usuario.primeiro_acesso == 'S'){
					req.session.usuarioLogado = usuario;
					res.render("admin/alterarSenha", {mensagemAlerta : undefined});
				}else{
					req.session.usuarioLogado = usuario;
					res.redirect('/?operSucesso=true');
				}
			}else{
				var mensagemAlerta = new application.app.util.MensagemAlerta(0, [{msg: 'Usuário não encontrado Verifique o telefone e senha informados.'}]);
				res.render("admin/login", {mensagemAlerta : mensagemAlerta});
			}

		})
		.catch(function(erro){
			console.log(erro);
			var mensagemAlerta = new application.app.util.MensagemAlerta(0, [{msg: 'Erro interno!!!!'}]);
			res.render("admin/login", {mensagemAlerta : mensagemAlerta});
		});
}

module.exports.alterarSenha = function(application, req, res){
	
	var novaSenha = req.body.novaSenha;
	var usuario = req.session.usuarioLogado;

	req.assert('novaSenha','Senha é obrigatório').notEmpty();
	req.assert('novaSenha','Senha deve ter entre 5 e 45 dígitos.').len(5, 45);
	
	var erros = req.validationErrors();

	// Erros de validação
	if(erros){
		var mensagemAlerta = new application.app.util.MensagemAlerta(0, erros);			
		res.render("admin/alterarSenha", {mensagemAlerta: mensagemAlerta});
		return;
	}

	var connection = application.config.dbConnection();

	let primeiroAcesso = 'N';
	application.app.dal.usuarioDAO.alterarSenha(connection, usuario.id, novaSenha, primeiroAcesso)
		.then(function(){
			usuario.primeiro_acesso = 'N';
			req.session.usuarioLogado = usuario;
			res.redirect('/?operSucesso=true');
		})
		.catch(function(erro){
			console.log(erro);
			var mensagemAlerta = new application.app.util.MensagemAlerta(0, [{msg: 'Erro interno!!!!'}]);
			res.render("admin/alterarSenha", {mensagemAlerta : mensagemAlerta});
		});
}


module.exports.logout = function(application, req, res){
	req.session.destroy(function(erro){
		res.redirect('/?operSucesso=true');
	});
}