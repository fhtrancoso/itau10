//  var usuarioDao = require('../dal/usuarioDAO');
//  var presencaDao = require('../dal/presencaDAO');

module.exports.cadastrarUsuarioPage = function(application, req, res){
	res.render("admin/cadastrarUsuario", {mensagemAlerta : undefined, usuario : {}});
}

module.exports.editarUsuarioPage = function(application, req, res){
	var idUsuario = req.query.idUsuario;

	var connection = application.config.dbConnection();

	var usuario;
	application.app.dal.usuarioDAO.getUsuarioPorId(idUsuario, connection)
	.then(function(result){
		usuario = result;
		res.render("admin/cadastrarUsuario", {mensagemAlerta : undefined, usuario : usuario});
	})
	.catch(function(erro){
		console.log(erro);
		var mensagemAlerta = new application.app.util.MensagemAlerta(0, [{msg: 'Erro interno!!!!'}]);
		res.render("admin/cadastrarUsuario", {mensagemAlerta : mensagemAlerta, usuario : {}});
	});
}

module.exports.salvarUsuario = function(application, req, res){
	var usuario = req.body;
	usuario.telefone = usuario.telefone.replace('-', '');

	req.assert('nome','Nome é obrigatório').notEmpty();
	req.assert('nome','Nome deve conter entre 3 e 40 caracteres').len(3, 40);
	req.assert('telefone','Telefone é obrigatório').notEmpty();
	req.assert('telefone','Telefone deve ter 9 dígitos.').len(9, 9);
	
	var erros = req.validationErrors();

	// Erros de validação
	if(erros){
		var mensagemAlerta = new application.app.util.MensagemAlerta(0, erros);			
		res.render("admin/cadastrarUsuario", {mensagemAlerta: mensagemAlerta, usuario : usuario});
		return;
	}

	// Se não houver id, é porque é inclusão e seta a senha padrão.
	if(!usuario.id){
		usuario.senha = 'itau10';
		usuario.primeiro_acesso = 'S';
	}
	usuario.pelada_id = 1;
	usuario.nome = usuario.nome.toUpperCase();

	var connection = application.config.dbConnection();

	application.app.dal.usuarioDAO.salvarUsuario(connection, usuario)
		.then(function(result){
			res.redirect('/?operSucesso=true');
		})
		.catch(function(erro){
			var mensagemAlerta = new application.app.util.MensagemAlerta(0, [{msg: 'Erro interno!!!!'}]);
			console.log(erro);
			res.render("admin/cadastrarUsuario", {mensagemAlerta : mensagemAlerta, usuario : usuario});
		})
}

module.exports.cadastrarPresencaPage = function(application, req, res){
	res.render("admin/cadastrarPresenca", {mensagemAlerta: undefined, usuario : {}});
}

module.exports.cadastrarPresenca = function(application, req, res){
	var presenca = req.body;

	req.assert('data','Data é obrigatório.').notEmpty();
	req.assert('data','Formato de data inválido.').isDate({format: "YYYY-MM-DD"});

	var erros = req.validationErrors();

	if(erros){
		var mensagemAlerta = new application.app.util.MensagemAlerta(0, erros);			
		res.render("admin/cadastrarPresenca", {mensagemAlerta: mensagemAlerta, presenca : presenca});
		return;
	}
	
	presenca.pelada_id = 1;
	presenca.status = 'A';
	presenca.ano = new Date(presenca.data).getFullYear();

	var connection = application.config.dbConnection();

	application.app.dal.presencaDAO.validaDataPresencaExistente(connection, presenca.data)
	.then(function(existe){
		if(!existe){

			var connection = application.config.dbConnection();
			application.app.dal.presencaDAO.getPresencaAberta(connection)
			.then(function(presencaAberta){
				if(!presencaAberta){
					console.log('Passou 002');
					var connection = application.config.dbConnection();
					application.app.dal.presencaDAO.inserirListaPresencaTransacional(connection, presenca)
						.then(function(){
							res.redirect('/?operSucesso=true');
						}).catch(function(error){
							var mensagemAlerta = new application.app.util.MensagemAlerta(0, [{msg: 'Erro interno!!!!'}]);
							res.render("admin/cadastrarPresenca", {mensagemAlerta : mensagemAlerta, presenca : presenca});
						});
					
				}else{
					var mensagemAlerta = new application.app.util.MensagemAlerta(0, [{msg: 'Já existe uma Data de Presença Aberta.'}]);
					res.render("admin/cadastrarPresenca", {mensagemAlerta : mensagemAlerta, presenca : presenca});
				}
			})
			.catch(function(erro){
				var mensagemAlerta = new application.app.util.MensagemAlerta(0, [{msg: 'Erro interno!!!!'}]);
				console.log(erro);
				res.render("admin/cadastrarPresenca", {mensagemAlerta : mensagemAlerta, presenca : presenca});
			});	
			
		}else{
			var mensagemAlerta = new application.app.util.MensagemAlerta(0, [{msg: 'Esta Data de presença já existe.'}]);
			res.render("admin/cadastrarPresenca", {mensagemAlerta : mensagemAlerta, presenca : presenca});
		}
	});	
}

module.exports.loginPage = function(application, req, res){
	res.render("admin/login", {mensagemAlerta : undefined, usuario : {}});
}

module.exports.deletarUsuario = function(application, req, res){
	var idUsuario = req.body.idUsuario;

	var connection = application.config.dbConnection();
	application.app.dal.usuarioDAO.deletarUsuarioTransacional(connection, idUsuario)
	.then(function(result){
		res.redirect('/?operSucesso=true');
	})
	.catch(function(erro){
		var mensagemAlerta = new application.app.util.MensagemAlerta(0, [{msg: 'Erro interno!!!!'}]);
		console.log(erro);
		res.render("usuario/listaUsuarios", {mensagemAlerta : mensagemAlerta, usuarios : usuarios});
	});
}

module.exports.resetarSenhaUsuario = function(application, req, res){
	var idUsuario = req.body.idUsuario;
	let senhaPadrao = 'itau10';
	let primeiroAcesso = 'S';

	var connection = application.config.dbConnection();
	application.app.dal.usuarioDAO.alterarSenha(connection, idUsuario, senhaPadrao, primeiroAcesso)
	.then(function(result){
		res.redirect('/?operSucesso=true');
	})
	.catch(function(erro){
		var mensagemAlerta = new application.app.util.MensagemAlerta(0, [{msg: 'Erro interno!!!!'}]);
		console.log(erro);
		res.render("usuario/listaUsuarios", {mensagemAlerta : mensagemAlerta, usuarios : usuarios});
	});
}

module.exports.fecharPresenca = function(application, req, res){
	var idPresenca = req.body.idPresenca;

	var connection = application.config.dbConnection();
	application.app.dal.presencaDAO.atualizarStatusPresenca(connection, 'F', idPresenca)
	.then(function(result){
		res.redirect('/?operSucesso=true');
	})
	.catch(function(erro){
		var mensagemAlerta = new application.app.util.MensagemAlerta(0, [{msg: 'Erro interno!!!!'}]);
		console.log(erro);
		res.render("presenca/listaPresencas", {mensagemAlerta : mensagemAlerta, listaPresencas : []});
	});
}