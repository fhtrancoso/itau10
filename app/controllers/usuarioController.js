// Carregar tela
module.exports.listarUsuarios = function(application, req, res){

	var connection = application.config.dbConnection();
	
	application.app.dal.usuarioDAO.getListaUsuarios(req, connection)
		.then(function(usuarios){

			var mensagemAlerta;
			res.render("usuario/listaUsuarios", {listaUsuarios : usuarios, mensagemAlerta: mensagemAlerta});
        })
		.catch(function(erro){
            console.log(erro);
			var mensagemAlerta = new application.app.util.MensagemAlerta(0, [{msg: 'Erro interno!!!!'}]);
			res.render("usuarios/listaUsuarios", {mensagemAlerta : mensagemAlerta, listaUsuarios : []});
		});	
}