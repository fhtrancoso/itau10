module.exports = function(application){

	application.get('/listarUsuarios', function(req, res){
        application.app.controllers.usuarioController.listarUsuarios(application, req, res);
    });

    application.get('/perfilUsuario', function(req, res){
        if(req.session.usuarioLogado != undefined){
            res.render("usuario/perfilUsuario", {mensagemAlerta: undefined});
		}else{
			res.redirect('/?mensagemErro=Realize login no sistema.');	
		}
    });
}