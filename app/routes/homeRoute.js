module.exports = function(application){

	application.get('/', function(req, res){
		application.app.controllers.homeController.index(application, req, res);
	});

	application.get('/escalacao', function(req, res){
		application.app.controllers.homeController.escalacao(application, req, res);
	});

	application.post('/autenticar', function(req, res){
		application.app.controllers.homeController.autenticar(application, req, res);
	});

	application.post('/alterarSenha', function(req, res){
		application.app.controllers.homeController.alterarSenha(application, req, res);
	});

	application.get('/logout', function(req, res){
		application.app.controllers.homeController.logout(application, req, res);
	});
}