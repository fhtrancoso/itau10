module.exports = function(application){

	application.get('/listarPresencas', function(req, res){
		if(req.session.usuarioLogado != undefined){
			application.app.controllers.presencaController.listarPresencas(application, req, res);
		}else{
			res.redirect('/?mensagemErro=Realize login no sistema.');	
		}
	});

	application.get('/usuariosPresenca', function(req, res){
		if(req.session.usuarioLogado != undefined){
			application.app.controllers.presencaController.listarUsuariosPresenca(application, req, res);
		}else{
			res.redirect('/?mensagemErro=Realize login no sistema.');	
		}
	});

	// Rota para realizar a ação de salvar lista de presença dos usuários
	application.post('/salvarListaUsuariosPresenca', function(req, res){
		if(req.session.usuarioLogado != undefined){
			application.app.controllers.presencaController.salvarListaUsuariosPresenca(application, req, res);
		}else{
			res.redirect('/?mensagemErro=Realize login no sistema.');	
		}
	});

	application.get('/presencaPendente', function(req, res){
		if(req.session.usuarioLogado != undefined){
			application.app.controllers.presencaController.listarUsuariosPresencaAberta(application, req, res);
		}else{
			res.redirect('/?mensagemErro=Realize login no sistema.');	
		}
	});

	application.get('/listarPresencasUsuario', function(req, res){
        application.app.controllers.presencaController.listarPresencasUsuario(application, req, res);
    });
}