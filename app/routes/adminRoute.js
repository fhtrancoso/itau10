module.exports = function(application){
	
	// Método para chamar a página
	application.get('/cadastrarUsuarioPage', function(req, res){	
		
		if(req.session.usuarioLogado != undefined){
			application.app.controllers.adminController.cadastrarUsuarioPage(application, req, res);
		}else{
			res.redirect('/?mensagemErro=Realize login no sistema.');	
		}
	});

	application.get('/editarUsuarioPage', function(req, res){	
		if(req.session.usuarioLogado != undefined){
			application.app.controllers.adminController.editarUsuarioPage(application, req, res);
		}else{
			res.redirect('/?mensagemErro=Realize login no sistema.');	
		}	
	});

	// Rota para realizar a ação de cadastrar
	application.post('/cadastrarUsuario', function(req, res){
		if(req.session.usuarioLogado != undefined){
			application.app.controllers.adminController.salvarUsuario(application, req, res);
		}else{
			res.redirect('/?mensagemErro=Realize login no sistema.');	
		}
	});

	// Método para chamar a página
	application.get('/cadastrarPresencaPage', function(req, res){
		if(req.session.usuarioLogado != undefined){
			application.app.controllers.adminController.cadastrarPresencaPage(application, req, res);
		}else{
			res.redirect('/?mensagemErro=Realize login no sistema.');	
		}
	});

	// Rota para realizar a ação de cadastrar
	application.post('/cadastrarPresenca', function(req, res){
		if(req.session.usuarioLogado != undefined){
			application.app.controllers.adminController.cadastrarPresenca(application, req, res);
		}else{
			res.redirect('/?mensagemErro=Realize login no sistema.');	
		}
	});

	// Método para chamar a página
	application.get('/loginPage', function(req, res){
		application.app.controllers.adminController.loginPage(application, req, res);
	});

	// Rota para realizar a ação de deletar um usuário por ID
	application.post('/deletarUsuario', function(req, res){
		if(req.session.usuarioLogado != undefined){
			application.app.controllers.adminController.deletarUsuario(application, req, res);
		}else{
			res.redirect('/?mensagemErro=Realize login no sistema.');	
		}
	});

	// Rota para realizar a ação de deletar um usuário por ID
	application.post('/resetarSenhaUsuario', function(req, res){
		if(req.session.usuarioLogado != undefined && req.session.usuarioLogado.perfil == 'A'){
			application.app.controllers.adminController.resetarSenhaUsuario(application, req, res);
		}else{
			res.redirect('/?mensagemErro=Realize login no sistema.');	
		}
	});

	// Rota para realizar a ação de fechar as presenças de determinada data
	application.post('/fecharPresenca', function(req, res){
		if(req.session.usuarioLogado != undefined){
			application.app.controllers.adminController.fecharPresenca(application, req, res);
		}else{
			res.redirect('/?mensagemErro=Realize login no sistema.');	
		}
	});
}