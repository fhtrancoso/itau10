insert into pelada (nome, local) values ("Pelada Itau 10", "Clube AASBEMGE");
select * from pelada;

select * from usuario;
insert into usuario (telefone, senha, nome, pelada_id) values (999999999, "itau10", "Filipe", 1);
insert into usuario (telefone, senha, nome, pelada_id) values (988888888, "itau10", "Diego", 1);
insert into usuario (telefone, senha, nome, pelada_id) values (977777777, "itau10", "Tonel", 1);
insert into usuario (telefone, senha, nome, pelada_id) values (966666666, "itau10", "Pudim", 1);

insert into usuario (telefone, senha, nome, pelada_id) values (955555555, "itau10", "Cabeça", 1);
insert into usuario (telefone, senha, nome, pelada_id) values (944444444, "itau10", "Dolar", 1);
insert into usuario (telefone, senha, nome, pelada_id) values (933333333, "itau10", "Lambreta", 1);
insert into usuario (telefone, senha, nome, pelada_id) values (922222222, "itau10", "Hulk", 1);

select * from presenca;
insert into presenca (data, situacao, ano, usuario_id, usuario_pelada_id) values (sysdate(), 1, 2018, 1, 1);
insert into presenca (data, situacao, ano, usuario_id, usuario_pelada_id) values (sysdate(), 1, 2018, 2, 1);
insert into presenca (data, situacao, ano, usuario_id, usuario_pelada_id) values (sysdate(), 1, 2018, 3, 1);
insert into presenca (data, situacao, ano, usuario_id, usuario_pelada_id) values (sysdate(), 0, 2018, 4, 1);

select * from presenca_retroativa;
insert into presenca_retroativa (ano, percentual, usuario_id, usuario_pelada_id) values (2017, 87.5, 1,1);