CREATE DEFINER=`root`@`localhost` PROCEDURE `PR_RELATORIO_ESCALACAO`(IN ano INT (4))
BEGIN
  DECLARE done INT DEFAULT FALSE;
  DECLARE vnome CHAR(45);
  DECLARE vidUsuario INT;  
  DECLARE cur1 CURSOR FOR SELECT id,nome FROM usuario;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  
  DROP TEMPORARY TABLE IF EXISTS temp;
  
  CREATE TEMPORARY TABLE temp
  (
		idUsuario int not null, 
        nome varchar(45) not null, 
        percent6 decimal(6,2) not null, 
        percentAno decimal(6,2) not null,
        percentAnoM1 decimal(6,2) not null,
        percentAnoM2 decimal(6,2) not null,
        ultimaListaPresenca varchar(1),
        ultimaListaPresencaOrdena int,
        presencaAtual varchar(1),
        presencaAtualOrdena int,
        PRIMARY KEY(idUsuario)
	);
  
  OPEN cur1;

  read_loop: LOOP
    FETCH cur1 INTO vidUsuario, vnome;
    
    IF done THEN
      LEAVE read_loop;
    END IF;
    
    
    -- CALCULANDO PERCENTUAL DAS ÚLTIMAS 6 PELADAS
    SELECT 
		ifnull(round((COUNT(*) / 6 * 100) , 0) , 0) INTO @percent6
	FROM
		(SELECT 
			pu.presente, pu.atrasado, pu.suspenso, pu.usuario_id
		FROM
			presenca_usuario pu
		JOIN presenca p ON p.id = pu.presenca_id
		WHERE
			pu.usuario_id = vidUsuario
            and p.status = 'F'
		ORDER BY p.data DESC
		LIMIT 6) a
	WHERE
		a.presente = 1 AND a.atrasado = 0;
        
        
	-- CALCULANDO PERCENTUAL DO ANO ATUAL
    SELECT 
            ifnull(round((COUNT(*) / (SELECT COUNT(*) FROM presenca p WHERE p.ano = ano and p.status = 'F') * 100) , 0) , 0) INTO @percentAno
        FROM
            presenca_usuario pu
		JOIN presenca p ON p.id = pu.presenca_id
        WHERE
            pu.usuario_id = vidUsuario 
				AND pu.presente = 1
                AND pu.atrasado = 0
				AND p.status = 'F'
                AND p.ano = ano;
                
	-- OBTENDO PERCENTUAL DE PRESENCAS RETROATIVAS ANO PASSADO
    IF EXISTS (select * from presenca_retroativa pr where pr.ano = (ano - 1) and pr.usuario_id = vidUsuario) 
    THEN
    
		select percentual into @percentAnoPassado
			from presenca_retroativa pr 
			where pr.ano = (ano - 1) 
				and pr.usuario_id = vidUsuario;
		 
    ELSE 
                
        SELECT 
            ifnull(round((COUNT(*) / (SELECT COUNT(*) FROM presenca p WHERE p.ano = (ano -1) and p.status = 'F') * 100) , 0) , 0) INTO @percentAnoPassado
        FROM
            presenca_usuario pu
		JOIN presenca p ON p.id = pu.presenca_id
        WHERE
            pu.usuario_id = vidUsuario 
				AND pu.presente = 1
                AND pu.atrasado = 0
				AND p.status = 'F'     
                AND p.ano = (ano - 1);
    END IF;
    
    -- OBTENDO PERCENTUAL DE PRESENCAS RETROATIVAS ANO RETRASADO
    IF EXISTS (select * from presenca_retroativa pr where pr.ano = (ano - 2) and pr.usuario_id = vidUsuario) 
    THEN
    
		select percentual into @percentAnoRetrasado
			from presenca_retroativa pr 
			where pr.ano = (ano - 2) 
				and pr.usuario_id = vidUsuario;
		 
    ELSE 
                
        SELECT 
            ifnull(round((COUNT(*) / (SELECT COUNT(*) FROM presenca p WHERE p.ano = (ano -2) and p.status = 'F') * 100) , 0) , 0) INTO @percentAnoRetrasado
        FROM
            presenca_usuario pu
		JOIN presenca p ON p.id = pu.presenca_id
        WHERE
            pu.usuario_id = vidUsuario 
				AND pu.presente = 1
                AND pu.atrasado = 0
				AND p.status = 'F'
                AND p.ano = (ano - 2);
                
    END IF;
                
    -- OBTENDO A ÚTLIMA PRESENÇA (OU FALTA, OU ATRASO, OU SUSPENSÃO) DOS USUÁRIOS
    SELECT 
            CASE
                    WHEN pu.suspenso = 1 THEN 'S'
                    WHEN pu.atrasado = 1 THEN 'A'
                    WHEN pu.presente = 1 AND pu.atrasado = 0 AND pu.suspenso = 0 THEN 'P'
                    WHEN pu.presente = 0 and pu.atrasado = 0 and pu.suspenso = 0 THEN 'F'
                    ELSE ' - '
                END status INTO @ultimaListaPresenca
        FROM
            presenca_usuario pu
                JOIN
            presenca p ON p.id = pu.presenca_id
        WHERE
            pu.usuario_id = vidUsuario
				AND p.status = 'F'
                AND p.data = (SELECT 
                    MAX(p.data)
                FROM
                    presenca p where p.status = 'F');
	
    -- OBTENDO A ÚTLIMA PRESENÇA (OU FALTA, OU ATRASO, OU SUSPENSÃO) DOS USUÁRIOS PARA ORDENAÇÃO
    SELECT 
            CASE
				WHEN pu.suspenso = 1 THEN 40
				WHEN pu.atrasado = 1 THEN 20
				WHEN pu.presente = 1 AND pu.atrasado = 0 AND pu.suspenso = 0 THEN 10
				WHEN pu.presente = 0 and pu.atrasado = 0 and pu.suspenso = 0 THEN 30
				ELSE 50
			END status INTO @ultimaListaPresencaOrdena
        FROM
            presenca_usuario pu
                JOIN
            presenca p ON p.id = pu.presenca_id
        WHERE
            pu.usuario_id = vidUsuario
				AND p.status = 'F'
                AND p.data = (SELECT 
                    MAX(p.data)
                FROM
                    presenca p where p.status = 'F');
    
    
    
    
    
    
    -- OBTENDO A PRESENÇA ATUAL (OU FALTA, OU ATRASO, OU SUSPENSÃO) DOS USUÁRIOS
    IF EXISTS (select * from presenca p where p.ano = ano and p.status = 'A') 
    THEN
		
        SELECT 
            CASE
                    WHEN pu.suspenso = 1 THEN 'S'
                    WHEN pu.atrasado = 1 THEN 'A'
                    WHEN pu.presente = 1 AND pu.atrasado = 0 AND pu.suspenso = 0 THEN 'P'
                    WHEN pu.presente = 0 and pu.atrasado = 0 and pu.suspenso = 0 THEN 'F'
                    ELSE ' - '
                END status INTO @presencaAtual
        FROM
            presenca_usuario pu
                JOIN
            presenca p ON p.id = pu.presenca_id
        WHERE
            pu.usuario_id = vidUsuario
				AND p.status = 'A'
                AND p.data = (SELECT 
                    MAX(p.data)
                FROM
                    presenca p where p.status = 'A');
    ELSE 
           SELECT '-' INTO @presencaAtual;        
    END IF;
    
    
    -- OBTENDO A PRESENÇA ATUAL (OU FALTA, OU ATRASO, OU SUSPENSÃO) DOS USUÁRIOS PARA ORDENAÇÃO
    IF EXISTS (select * from presenca p where p.ano = ano and p.status = 'A') 
    THEN
		
        SELECT 
            CASE
				WHEN pu.suspenso = 1 THEN 40
				WHEN pu.atrasado = 1 THEN 20
				WHEN pu.presente = 1 AND pu.atrasado = 0 AND pu.suspenso = 0 THEN 10
				WHEN pu.presente = 0 and pu.atrasado = 0 and pu.suspenso = 0 THEN 30
				ELSE 50
			END status INTO @presencaAtualOrdena
        FROM
            presenca_usuario pu
                JOIN
            presenca p ON p.id = pu.presenca_id
        WHERE
            pu.usuario_id = vidUsuario
				AND p.status = 'A'
                AND p.data = (SELECT 
                    MAX(p.data)
                FROM
                    presenca p where p.status = 'A');
    ELSE 
           SELECT 0 INTO @presencaAtualOrdena;
    END IF;
    

    
    insert into temp 
		values 
        (vidUsuario, 
			vnome, 
            @percent6, 
            @percentAno, 
            @percentAnoPassado, 
            @percentAnoRetrasado, 
            @ultimaListaPresenca, 
            @ultimaListaPresencaOrdena,
            @presencaAtual, 
            @presencaAtualOrdena);
    
  END LOOP;

  CLOSE cur1;
  
  select * from temp t
  where exists (select usuario_id from presenca_usuario pu 
					join presenca p on p.id = pu.presenca_id
					where pu.usuario_id = t.idUsuario and p.status = 'F')
  order by 
		t.presencaAtualOrdena, 
		t.ultimaListaPresencaOrdena, 
		t.percent6 desc, 
        t.percentAno desc, 
        t.percentAnoM1 desc, 
        t.percentAnoM2 desc, 
        t.nome;
  
  drop table temp;
END